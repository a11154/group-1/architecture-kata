
# Comercio electrónico y sistema de gestión de contenidos


# Autores
- Jhony Llano
- Cristian Cazares
- José Castillo
- Fausto Amaguaña

# Descripción

Este es la solución para un sitio web de comercio electrónico y de un gestor de contenidos. 

Las actividades en la aplicación web del comercio electrónico son las siguientes:
- Registrar usuario
- Mostrar catálogo
- Comprar Libro
- Buscar Libro
- El formato del libro (digital o físico)
- Ver pedidos
- Cancelar pedidos

Las actividades en la aplicación del gestor de contenidos giran alrededor del contenido de los libros, entre las cuales tenemos:
- Crear
- Revisar
- Publicar

Los administradores tienen las siguientes actividades.
- Notificación de nuevos capítulos, libros
- Administrar la configuración de pago
- Publicar libros en el catalogo

Adicionalmente la solución notificará a los usuarios de nuevo contenido/catálogo por correo electrónico

# Decisiones de arquitectura


La documentación de decisiones de arquitectura o Architecture Decision Records (ADR) es una colección de documentos que recogen individualmente cada una de las decisiones de arquitectura tomadas. 

|  A continuación las de esta solución |
|---|
| [Lenguaje para escribir documentos](adr/idioma-a-utilizar-para-documentar/index.md)|
| [Arquitectura de la solución](adr/microservicios/index.md) |
| [Repositorio de archivos](adr/file-storage/index.md) |
| [Repositorio de datos](adr/mongo/index.md) |
| [Interfaz de usuario](adr/frontend-a-utilizar/index.md) |


# Diagramas
Los diagramas estarán plasmados por el [modelo c4](https://c4model.com/)
Estos diagramas proporcionan diferentes niveles de abstracción, cada uno de los cuales es relevante para una audiencia diferente.

|  A continuación los de esta solución |

- [contexto](diagrama/diagrama-context.dsl)
- [contenedores](diagrama/diagrama-context.dsl)
- [componentes](diagrama/diagrama-context.dsl)


# Pila de tecnología
- Node JS
- Express JS
- Angular
- Ionic
- Flutter
- MongoDB
- Bootstrap
