# Frontend a utlizar

### Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
El cliente necesita una interfaz donde los clientes desean comprar los libros prublicados tanto como versiones betas como finales, y como uno de los requerimientos es, se necesita tener todo con la mejor tecnologia, se lo haria en hibrida para poder tener todo en una sola codificación para que sea Web y Movil. Se tiene separado el frontend con el Core por la razon de que esta interfaz va tener un gran trafico de usuarios y para mejorar la experiencia del usuario se lo separo.

## Decisión
Programacion con Framework Hibrido (IONIC, FLUTTER), y compilar tanto para la web y dispositivos móviles.

## Consecuencia
* Un repositorio mas para el proyecto donde se aloja el forntend
* Adquirir licencia de desarrollo para publicar en tiendas Andorid y Ios
* Capacitar al equipo de trabajo para utilizar (IONIC, FLUTTER)

## Estado
Aprobado