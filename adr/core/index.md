# Core a utilizar

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
La plataforma no tiene un alto trafico de usuarios.
Debe admitir al menos 3 roles:
* clientela
* autores
* editores/revisores

Sin embargo, está claro a partir de la descripción que también tendríamos:

* Ingenieros de soporte (para ayudar a los clientes a resolver sus problemas)


Funcionalidad

* CMS
* Servicion rest para el Frontend
* Servicios de Integración
* Flujo de publicación y revisión
* Frontend CMS



## Decisión
Se utiliza un core para realizar la administranción del sistema para editores, administradores de contenido, ingenieros de soporte y otros.

Tambien se utilizara el core para exponer servicios de integración tanto para el ecommerce, microservicios,servicioes externos de integración (Mainframe).  

Se implemetan el Frontend del CMS ya que el trafico de usuarios no tiene mucha demanda.

## Consecuencia
* Contratar servicio en la nube con gran cantidad de procesamiento y memoria ram.
* Como se encuentra todo desarrollado en el core, el tiempo para la implementación del CMS es menor.


## Estado
Aprobado