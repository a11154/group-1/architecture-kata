# Repositorio de datos para archivos

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
Necesitamos un repositorio para gestionar los contenidos de las obras de los autores en archivos pdf
- Revisión
- Publicación
- Distribución

## Decisión
Optamos por usar S3 de Amazon debido a la experticia del equipo y por la fiabilidad del servicio.

## Consecuencia
Reduccion de costos operativos
Contratar un servicio en la nube

## Estado
Aprobado