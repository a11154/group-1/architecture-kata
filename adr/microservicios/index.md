# Mircoservicios

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
Se utiliza microservicios para Notificaciones, Editorias y consultas que permiten la integración continua, lo que facilita las pruebas, la actualización del código y acelera el tiempo de editorial de libros, notificaciones y nuevas busquedas o características.

## Decisión
Micro de servicio (Notificación) se utilizara SMTP
Micro de servicio (Editorial) Desarrollo del micro servicio con NODE JS
Micro de servicio (Procesar busqueda) Desarrollo del micro servicio con Redis

## Consecuencia
Configurar el SMTP
Desarrollo del micro servicio con NODE JS
Desarrollo del micro servicio con Redis
## Estado
Aprobado