# Repositorio de datos 

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
Necesitamos un repositorio para gestionar un alto volumen de datos:
- Ecomerce
- CMS

## Decisión
Optamos por MongoDB, por su gran capacidad de almacenamiento, que es altamente escalable y su flexibilidad

## Consecuencia
Reduccion de costos operativos
Contratar un servicio en la nube

## Estado
Aprobado