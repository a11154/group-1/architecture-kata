# Repositorio de datos cache

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
Necesitamos usar una base de datos de alto rendimiento para datos que serán solicitados con frecuencia:
- Busqueda de libros
- Busqueda de autores

## Decisión
Optamos por usar RedisDB por alto rendimiento y ayudrá a liberar la carga de trabajo de la base de datos principal.

## Consecuencia
Reduccion de costos operativos
Contratar un servicio en la nube

## Consecuencia
Aprobado