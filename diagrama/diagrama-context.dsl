workspace "Getting Started" "This is a model of my software system." {
    
    model {
        client = person "Client" "Usuario de Ecommerce."
        author = person "Autor" "Persona que crea el contenido."
        editor = person "Revisor-Editor" "Personas que realizan comentarios y publican."
        
        cmsSoftwareSystem = softwareSystem "CMS System" "CMS de Ecommerce."{
            spaAuthor = container "Single Page Application de Autor" "Funcionalidad para envio de libros a revisar y para aprobar los cambios de los editores" "Vue" "Web Browser"
            spaEditor = container "Single Page Application de Editor" "Funcionalidad para aprobar o editar los libros para epublicar" "Vue" "Web Browser"
            database = container "Base de Datos" "Almacenar la información de los libros" "PostgreSQL" "Database"
            apiAplication = container "Aplicacion API" "Publicacion REST API para realizar todas las funcionalidades del CMS" "Laravel" {
                authService = component "Servicio de autentificación CMS" "Auth Laravel"
                publishService = component "Servicio del proceso de publicación de libros" "Service Laravel"
                storeService = component "Servicio enviar a generar del proceso de publicación de libros" "Service Laravel"
                notificationService = component "Servicio enviar a notificar (publicaciones, ediciones, aprobaciones, y cambios)" "Service Laravel"
                searchService = component "Servicio Conexción de busqueda" "Service Laravel"
                printService = component "Servicio enviar imprimir libros" "Service Laravel"
                ecommerceService = component "Servicio de funciones Ecommerce para frontend" "Service Laravel"
            }
        }
        
        ecommerceSoftwareSystem = softwareSystem "Ecommerce System" "Sistema Ecommerce."{
            spaClient = container "Single Page Application de Ecommerce" "Frontend de Ecommerce" "IONIC" "Web Browser and Mobile"
        }
        
        mainframeSoftwareSystem = softwareSystem "Mainframe" "External software system." {
            tags ExternalSystem
        }
        
        servicesSoftwareSystem = softwareSystem "Micro servicios complementario" "Servicios complentario CMS."{
            microServicesAplication = container "Aplicacion Microservicios API" "Publicacion REST API funcionalidades complementarias" "Node Express" {
                storeMicroservice = component "Servicio generar y almacenar PDF" "Node Express"
                notificationMicroservice = component "Servicio envío de notificaciones" "Node Express"
                searchMicroservice = component "Servicio Conexción de busqueda" "Node Express"
                fileStore = component "File Store" "Almacenar los PDFs publicados" "Amazon S3" "File Store"
                redisStore = component "Redis Store" "Almacenamiento Redis para busqueda" "Amazon Elastic" "Redis Store"
            }  
        }
        
        client -> ecommerceSoftwareSystem "Buscar, Comprar, Descargar, Registrarse"
        author -> cmsSoftwareSystem "Crear, Modifican Contenido (Libros)"
        editor -> cmsSoftwareSystem "Realizan Comentarios, notificaciones sobre la revisión de las publicaciones"
        cmsSoftwareSystem -> mainframeSoftwareSystem "Envía la información necesaria para publicar, distribución, regalías, marketing."
        
        author -> spaAuthor "Enviar a revisar y aprobar cambios del Libro"
        editor -> spaEditor "Aprobar,Editar y Publicar Libro"
        client -> spaClient "Aprobar,Editar y Publicar Libro"
        spaAuthor -> apiAplication "Integracion REST Front y Back"
        spaEditor -> apiAplication "Integracion REST Front y Back"
        apiAplication -> database "Almacenar información CMS"
        apiAplication -> microServicesAplication "Conexión servicios complemantarios"
        spaClient -> apiAplication "Conexión REST funciones Ecommerce."
        
        notificationService -> notificationMicroservice "Conexión REST notifiaciones"
        storeService -> storeMicroservice "Conexión REST almacenar PDF"
        searchService -> searchMicroservice "Conexión REST busqueda de libros"
        
        storeMicroservice -> fileStore "Almacenar en S3 PDF libros"
        searchMicroservice -> redisStore "Busqueda de libros en memoria cache"
        
    }

    views {
        systemContext cmsSoftwareSystem "CMSSystem" "Nivel de contexto del CMS." {
            include *
            autoLayout lr
        }
        
        systemContext ecommerceSoftwareSystem "EcommerceSystem" "Nivel de contexto del Ecommerce." {
            include *
            autoLayout lr
        }

        container ecommerceSoftwareSystem "ContainerLevelEcommerce" "Nivel de contendor del Ecommerce."{
            include *
        }
        
        container cmsSoftwareSystem "ContainerLevelCms" "Nivel de contendor del CMS."{
            include *
        }
        
        component apiAplication "APIAplicationCms" "Aplicación API CMS"{
            include *
            autoLayout lr
        }
        
        component microServicesAplication "MicroServiceAplication" "Aplicación microservicos complementarios"{
            include *
            autoLayout lr
        }
        
        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "ExternalSystem" {
                background #808080
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Database" {
                shape Cylinder
            }
            element "File Store" {
                shape Cylinder
            }
            element "Redis Store" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }   
}